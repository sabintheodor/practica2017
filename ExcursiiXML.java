package excursiiafisare_xml_pdf;

/**
 *
 * @author Luca
 */

public class ExcursiiXML {
    
    private int ID;
    private String Descriere;
    private String DataEx;
    private String Durata;
    private String NLocuri;
    private String Pret;

    public void setID(int value){
       this.ID=value;
    }
    public void setDescriere(String value){
       this.Descriere=value;
    }    
    public void setDataEx(String value){
       this.DataEx=value;
    }    
    public void setDurata(String value){
       this.Durata=value;
    }
    public void setNLocuri(String value){
       this.NLocuri=value;
    }
    public void setPret(String value){
       this.Pret=value;
    }
    
    public int getID(){
       return this.ID;
    }
    public String getDescriere(){
       return this.Descriere;
    }    
    public String getDataEx(){
       return this.DataEx;
    }    
    public String getDurata(){
       return this.Durata;
    }
    public String getNLocuri(){
       return this.NLocuri;
    }
    public String getPret(){
       return this.Pret;
    }

}
