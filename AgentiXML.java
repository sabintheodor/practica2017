package afisare_xml_pdf;

/**
 *
 * @author Luca
 */

public class AgentiXML {
    
    private int ID;
    private String Nume;
    private String Prenume;
    private String Adresa;
    private String Oras;

    public void setID(int value){
       this.ID=value;
    }
    public void setNume(String value){
       this.Nume=value;
    }    
    public void setPrenume(String value){
       this.Prenume=value;
    }    
   
    public void setOras(String value){
       this.Oras=value;
    }
    public int getID(){
       return this.ID;
    }
    public String getNume(){
       return this.Nume;
    }    
    public String getPrenume(){
       return this.Prenume;
    }    
    
    public String getOras(){
       return this.Oras;
    }

}
