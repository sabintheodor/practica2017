package sql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
/**
 *
 * @author Luca
 */
public class Rezervari {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
  {
    try
    {
      // create our mysql database connection
      String myDriver = "org.gjt.mm.mysql.Driver";
      String myUrl = "jdbc:mysql://localhost:3306/planificare_excursie";
      Class.forName(myDriver);
      Connection conn = DriverManager.getConnection(myUrl, "root", "");
      
      // our SQL SELECT query. 
      // if you only need a few columns, specify them by name instead of using "*"
      String query = "SELECT * FROM rezervari";

      // create the java statement
      Statement st = conn.createStatement();
      
      // execute the query, and get a java resultset
      ResultSet rs = st.executeQuery(query);
      
      // iterate through the java resultset
      while (rs.next())
      {
        int idR = rs.getInt("cod rez");
        int idE = rs.getInt("cod ex");
        int idC = rs.getInt("cod client");
        int idA = rs.getInt("cod agent");
        String time = rs.getString("data rez");
        String nrLoc = rs.getString("nr locuri");

        
        // print the results
        System.out.format("%s, %s, %s, %s, %s, %s\n",idR, idE, idC, idA, time, nrLoc);
      }
      st.close();
    }
    catch (Exception e)
    {
      System.err.println("Got an exception! ");
      System.err.println(e.getMessage());
    }
  }
    
}
