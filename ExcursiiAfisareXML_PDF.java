package excursiiafisare_xml_pdf;

import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.sql.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import java.io.File;
import java.io.FileOutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Luca
 */

public class ExcursiiAfisareXML_PDF {
    
    protected ArrayList<ExcursiiXML> list_excursii = new ArrayList<ExcursiiXML>();
    protected String myDriver = "org.gjt.mm.mysql.Driver";
    protected String myUrl = "jdbc:mysql://localhost:3306/planificare_excursie";
    public void loaddata()
    {
        try
        {
          Class.forName(myDriver);
          Connection conn = DriverManager.getConnection(myUrl, "root", "");
          // create our mysql database connection

          // our SQL SELECT query. 
          // if you only need a few columns, specify them by name instead of using "*"
          String query = "SELECT * FROM Excursii";

          // create the java statement
          Statement st = conn.createStatement();

          // execute the query, and get a java resultset
          ResultSet rs = st.executeQuery(query);

          // iterate through the java resultset
          while (rs.next())
          {
            ExcursiiXML x=new ExcursiiXML();
            x.setID(rs.getInt("cod ex"));
            x.setDescriere(rs.getString("descriere"));
            x.setDataEx(rs.getString("data ex"));
            x.setDurata(rs.getString("durata"));
            x.setNLocuri(rs.getString("nr locuri"));
            x.setPret(rs.getString("pret"));
            list_excursii.add(x);
            
          }
          st.close();
        }
        catch (Exception e)
        {
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }

    /**
     *
     * @param filename
     */
    public void afisare_xml(String filename)
    {
        try{
            int i;
            DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = 
               dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("Vacanta");
            doc.appendChild(rootElement);
            for(i=0;i<list_excursii.size();i++){
                //  excursie element
                Element excursie = doc.createElement("excursie");
                rootElement.appendChild(excursie);

                // setting attribute to element
                Attr attr = doc.createAttribute("Cod");
                attr.setValue(Integer.toString(list_excursii.get(i).getID()));
                excursie.setAttributeNode(attr);

                // Descriere element
                Element descriere = doc.createElement("Descriere");
                descriere.appendChild(doc.createTextNode(list_excursii.get(i).getDescriere()));
                excursie.appendChild(descriere);
                
                // Data element
                Element data = doc.createElement("DataEx");
                data.appendChild(doc.createTextNode(list_excursii.get(i).getDataEx()));
                excursie.appendChild(data);
                
                // Durata element
                Element durata = doc.createElement("Durata");
                durata.appendChild(doc.createTextNode(list_excursii.get(i).getDurata()));
                excursie.appendChild(durata);
                
                // Nr Locuri element
                Element locuri = doc.createElement("Locuri");
                locuri.appendChild(doc.createTextNode(list_excursii.get(i).getNLocuri()));
                excursie.appendChild(locuri);
                
                // Pret element
                Element pret = doc.createElement("Pret");
                pret.appendChild(doc.createTextNode(list_excursii.get(i).getPret()));
                excursie.appendChild(pret);
            }

            // write the content into xml file
            TransformerFactory transformerFactory =
            TransformerFactory.newInstance();
            Transformer transformer =
            transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result =
            new StreamResult(new File(filename));
            transformer.transform(source, result);
        }
        catch (Exception e){
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }
    
    public void afisare_pdf(String filenamefrom, String filenameto)
    {
        int i;
        try{	
            File inputFile = new File(filenamefrom);
            DocumentBuilderFactory dbFactory 
               = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            com.itextpdf.text.Document document = new com.itextpdf.text.Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filenameto));
            writer.setPdfVersion(PdfWriter.VERSION_1_7);
            document.open();
            document.add(new Paragraph("Root element :" 
               + doc.getDocumentElement().getNodeName()));
            NodeList nList = doc.getElementsByTagName("excursie");
            document.add(new Paragraph("----------------------------"));
            document.add(new Paragraph("\n"));
            for (int temp = 0; temp < nList.getLength(); temp++) {
               Node nNode = nList.item(temp);
               document.add(new Paragraph("Current Element :" 
                  + nNode.getNodeName()));
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                  Element eElement = (Element) nNode;
                  document.add(new Paragraph("ID : " 
                     + eElement.getAttribute("Cod")));
                  document.add(new Paragraph("Descriere : " 
                     + eElement
                     .getElementsByTagName("Descriere")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("DataEx : " 
                  + eElement
                     .getElementsByTagName("DataEx")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Durata : " 
                  + eElement
                     .getElementsByTagName("Durata")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Nr Locuri : " 
                  + eElement
                     .getElementsByTagName("Locuri")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Pret : " 
                  + eElement
                     .getElementsByTagName("Pret")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("\n"));
            }
         }
            document.close();        
        }
        catch (Exception e)
        {
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }
     public static void main(String[] args) {
         ExcursiiAfisareXML_PDF control = new ExcursiiAfisareXML_PDF();
         control.loaddata();
         control.afisare_xml("excursii.xml");
         control.afisare_pdf("excursii.xml","excursii.pdf");
 }
}
