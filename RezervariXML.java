package rezervariafisare_xml_pdf;

/**
 *
 * @author Luca
 */

public class RezervariXML {
    
    private int ID;
    private String CodEx;
    private String CodClient;
    private String CodAgent;
    private String NLocuri;
    private String DataRez;

    public void setID(int value){
       this.ID=value;
    }
    public void setCodEx(String value){
       this.CodEx=value;
    }    
    public void setCodClient(String value){
       this.CodClient=value;
    }    
    public void setCodAgent(String value){
       this.CodAgent=value;
    }
    public void setNLocuri(String value){
       this.NLocuri=value;
    }
    public void setDataRez(String value){
       this.DataRez=value;
    }
    
    public int getID(){
       return this.ID;
    }
    public String getCodEx(){
       return this.CodEx;
    }    
    public String getCodClient(){
       return this.CodClient;
    }    
    public String getCodAgent(){
       return this.CodAgent;
    }
    public String getNLocuri(){
       return this.NLocuri;
    }
    public String getDataRez(){
       return this.DataRez;
    }

}
