package clientiafisare_xml_pdf;

import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.sql.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import java.io.File;
import java.io.FileOutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Luca
 */

public class ClientiAfisareXML_PDF {
    
    protected ArrayList<ClientiXML> list_clienti = new ArrayList<ClientiXML>();
    protected String myDriver = "org.gjt.mm.mysql.Driver";
    protected String myUrl = "jdbc:mysql://localhost:3306/planificare_excursie";
    public void loaddata()
    {
        try
        {
          Class.forName(myDriver);
          Connection conn = DriverManager.getConnection(myUrl, "root", "");
          // create our mysql database connection

          // our SQL SELECT query. 
          // if you only need a few columns, specify them by name instead of using "*"
          String query = "SELECT * FROM Clienti";

          // create the java statement
          Statement st = conn.createStatement();

          // execute the query, and get a java resultset
          ResultSet rs = st.executeQuery(query);

          // iterate through the java resultset
          while (rs.next())
          {
            ClientiXML x=new ClientiXML();
            x.setID(rs.getInt("cod client"));
            x.setNume(rs.getString("nume"));
            x.setPrenume(rs.getString("prenume"));
            x.setAdresa(rs.getString("adresa"));
            x.setOras(rs.getString("oras"));
            list_clienti.add(x);
            
          }
          st.close();
        }
        catch (Exception e)
        {
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }

    /**
     *
     * @param filename
     */
    public void afisare_xml(String filename)
    {
        try{
            int i;
            DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = 
               dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("Persoana");
            doc.appendChild(rootElement);
            for(i=0;i<list_clienti.size();i++){
                //  client element
                Element client = doc.createElement("client");
                rootElement.appendChild(client);

                // setting attribute to element
                Attr attr = doc.createAttribute("ID");
                attr.setValue(Integer.toString(list_clienti.get(i).getID()));
                client.setAttributeNode(attr);

                // Nume element
                Element nume = doc.createElement("Nume");
                nume.appendChild(doc.createTextNode(list_clienti.get(i).getNume()));
                client.appendChild(nume);
                
                // Prenume element
                Element prenume = doc.createElement("Prenume");
                prenume.appendChild(doc.createTextNode(list_clienti.get(i).getPrenume()));
                client.appendChild(prenume);
                
                // Adresa element
                Element adresa = doc.createElement("Adresa");
                adresa.appendChild(doc.createTextNode(list_clienti.get(i).getAdresa()));
                client.appendChild(adresa);
                
                // Oras element
                Element oras = doc.createElement("Oras");
                oras.appendChild(doc.createTextNode(list_clienti.get(i).getOras()));
                client.appendChild(oras);
            }

            // write the content into xml file
            TransformerFactory transformerFactory =
            TransformerFactory.newInstance();
            Transformer transformer =
            transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result =
            new StreamResult(new File(filename));
            transformer.transform(source, result);
        }
        catch (Exception e){
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }
    
    public void afisare_pdf(String filenamefrom, String filenameto)
    {
        int i;
        try{	
            File inputFile = new File(filenamefrom);
            DocumentBuilderFactory dbFactory 
               = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            com.itextpdf.text.Document document = new com.itextpdf.text.Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filenameto));
            writer.setPdfVersion(PdfWriter.VERSION_1_7);
            document.open();
            document.add(new Paragraph("Root element :" 
               + doc.getDocumentElement().getNodeName()));
            NodeList nList = doc.getElementsByTagName("client");
            document.add(new Paragraph("----------------------------"));
            document.add(new Paragraph("\n"));
            for (int temp = 0; temp < nList.getLength(); temp++) {
               Node nNode = nList.item(temp);
               document.add(new Paragraph("Current Element :" 
                  + nNode.getNodeName()));
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                  Element eElement = (Element) nNode;
                  document.add(new Paragraph("ID : " 
                     + eElement.getAttribute("ID")));
                  document.add(new Paragraph("Nume : " 
                     + eElement
                     .getElementsByTagName("Nume")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Prenume : " 
                  + eElement
                     .getElementsByTagName("Prenume")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Adresa : " 
                  + eElement
                     .getElementsByTagName("Adresa")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Oras : " 
                  + eElement
                     .getElementsByTagName("Oras")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("\n"));
            }
         }
            document.close();        
        }
        catch (Exception e)
        {
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }
     public static void main(String[] args) {
         ClientiAfisareXML_PDF control = new ClientiAfisareXML_PDF();
         control.loaddata();
         control.afisare_xml("clienti.xml");
         control.afisare_pdf("clienti.xml","clienti.pdf");
 }
}
