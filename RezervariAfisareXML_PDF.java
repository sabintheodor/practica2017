package rezervariafisare_xml_pdf;

import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import java.sql.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import java.io.File;
import java.io.FileOutputStream;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Luca
 */

public class RezervariAfisareXML_PDF {
    
    protected ArrayList<RezervariXML> list_rezervari = new ArrayList<RezervariXML>();
    protected String myDriver = "org.gjt.mm.mysql.Driver";
    protected String myUrl = "jdbc:mysql://localhost:3306/planificare_excursie";
    public void loaddata()
    {
        try
        {
          Class.forName(myDriver);
          Connection conn = DriverManager.getConnection(myUrl, "root", "");
          // create our mysql database connection

          // our SQL SELECT query. 
          // if you only need a few columns, specify them by name instead of using "*"
          String query = "SELECT * FROM Rezervari";

          // create the java statement
          Statement st = conn.createStatement();

          // execute the query, and get a java resultset
          ResultSet rs = st.executeQuery(query);

          // iterate through the java resultset
          while (rs.next())
          {
            RezervariXML x=new RezervariXML();
            x.setID(rs.getInt("cod rez"));
            x.setCodEx(rs.getString("cod ex"));
            x.setCodClient(rs.getString("cod client"));
            x.setCodAgent(rs.getString("cod agent"));
            x.setDataRez(rs.getString("data rez"));
            x.setNLocuri(rs.getString("nr locuri"));
            list_rezervari.add(x);
            
          }
          st.close();
        }
        catch (Exception e)
        {
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }

    /**
     *
     * @param filename
     */
    public void afisare_xml(String filename)
    {
        try{
            int i;
            DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = 
               dbFactory.newDocumentBuilder();
            Document doc = dBuilder.newDocument();
            // root element
            Element rootElement = doc.createElement("AgentieTurism");
            doc.appendChild(rootElement);
            for(i=0;i<list_rezervari.size();i++){
                //  rezervare element
                Element rezervare = doc.createElement("rezervare");
                rootElement.appendChild(rezervare);

                // setting attribute to element
                Attr attr = doc.createAttribute("CodRez");
                attr.setValue(Integer.toString(list_rezervari.get(i).getID()));
                rezervare.setAttributeNode(attr);

                // Cod Ex element
                Element codex = doc.createElement("CodEx");
                codex.appendChild(doc.createTextNode(list_rezervari.get(i).getCodEx()));
                rezervare.appendChild(codex);
                
                // Cod Client element
                Element codclient = doc.createElement("CodClient");
                codclient.appendChild(doc.createTextNode(list_rezervari.get(i).getCodClient()));
                rezervare.appendChild(codclient);
                
                // Cod Agent element
                Element codagent = doc.createElement("CodAgent");
                codagent.appendChild(doc.createTextNode(list_rezervari.get(i).getCodAgent()));
                rezervare.appendChild(codagent);
                
                // Nr Locuri element
                Element locuri = doc.createElement("Locuri");
                locuri.appendChild(doc.createTextNode(list_rezervari.get(i).getNLocuri()));
                rezervare.appendChild(locuri);
                
                // Data element
                Element datarez = doc.createElement("DataRez");
                datarez.appendChild(doc.createTextNode(list_rezervari.get(i).getDataRez()));
                rezervare.appendChild(datarez);
            }

            // write the content into xml file
            TransformerFactory transformerFactory =
            TransformerFactory.newInstance();
            Transformer transformer =
            transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result =
            new StreamResult(new File(filename));
            transformer.transform(source, result);
        }
        catch (Exception e){
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }
    
    public void afisare_pdf(String filenamefrom, String filenameto)
    {
        int i;
        try{	
            File inputFile = new File(filenamefrom);
            DocumentBuilderFactory dbFactory 
               = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            com.itextpdf.text.Document document = new com.itextpdf.text.Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filenameto));
            writer.setPdfVersion(PdfWriter.VERSION_1_7);
            document.open();
            document.add(new Paragraph("Root element :" 
               + doc.getDocumentElement().getNodeName()));
            NodeList nList = doc.getElementsByTagName("rezervare");
            document.add(new Paragraph("----------------------------"));
            document.add(new Paragraph("\n"));
            for (int temp = 0; temp < nList.getLength(); temp++) {
               Node nNode = nList.item(temp);
               document.add(new Paragraph("Current Element :" 
                  + nNode.getNodeName()));
               if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                  Element eElement = (Element) nNode;
                  document.add(new Paragraph("Cod Rezervare : " 
                     + eElement.getAttribute("CodRez")));
                  document.add(new Paragraph("Cod Excursie : " 
                     + eElement
                     .getElementsByTagName("CodEx")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Cod Client : " 
                  + eElement
                     .getElementsByTagName("CodClient")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Cod Agent : " 
                  + eElement
                     .getElementsByTagName("CodAgent")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Nr. Locuri : " 
                  + eElement
                     .getElementsByTagName("Locuri")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("Data Rezervare : " 
                  + eElement
                     .getElementsByTagName("DataRez")
                     .item(0)
                     .getTextContent()));
                  document.add(new Paragraph("\n"));
            }
         }
            document.close();        
        }
        catch (Exception e)
        {
          System.err.println("Got an exception! ");
          System.err.println(e.getMessage());
        }
    }
     public static void main(String[] args) {
         RezervariAfisareXML_PDF control = new RezervariAfisareXML_PDF();
         control.loaddata();
         control.afisare_xml("rezervari.xml");
         control.afisare_pdf("rezervari.xml","rezervari.pdf");
 }
}
